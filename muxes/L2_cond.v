module L2_cond (    input clk,
                    input selector,
                    input reset_L,
                    input [7:0] data_in0,
                    input [7:0] data_in1,
                    input valid_0,
                    input valid_1,
                    output valid_out,
                    output [7:0] data_out_cond
                );

    mux21_8b_cond mux1(
        .clk        (clk),
        .reset_L    (reset_L),
        .selector   (selector),
        .data_in0   (data_in0[7:0]),
        .data_in1   (data_in1[7:0]),
        .valid_0    (valid_0),
        .valid_1    (valid_1),
        .valid_out  (valid_out),
        .data_out_cond (data_out_cond[7:0])
    );
endmodule