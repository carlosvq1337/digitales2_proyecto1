module mux21_8b_cond (  input clk,
                        input reset_L,
                        input selector,
                        input [7:0] data_in0,
                        input valid_0,
                        input [7:0] data_in1,
                        input valid_1,
                        output reg valid_out,
                        output reg[7:0] data_out_cond
                    );

    reg[7:0] salMux1;
    reg validInter;

    always @(*) begin
        if (selector == 0) begin
            validInter = valid_1;
            salMux1 = data_in1;
        end else begin
            validInter = valid_0;
            salMux1 = data_in0;
        end
    end

    always @(posedge clk) begin
        if (reset_L == 1) begin
            valid_out <= validInter;
            if(validInter == 1) begin
                data_out_cond <= salMux1;
                
            end
        end else begin
            data_out_cond <= 8'h00;
            valid_out <= 0;
        end
    end
endmodule