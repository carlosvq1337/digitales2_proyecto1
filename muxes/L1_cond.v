module L1_cond(     input clk_2f,
                    input clk_f,
                    input reset_L,
                    input [7:0] data_in0,
                    input [7:0] data_in1,
                    input [7:0] data_in2,
                    input [7:0] data_in3,
                    input valid_in0,
                    input valid_in1,
                    input valid_in2,
                    input valid_in3,
                    output valid_out0_toL2,
                    output valid_out1_toL2,
                    output [7:0] out0_toL2,
                    output [7:0] out1_toL2
                );

    mux21_8b_cond mux0(
        .clk        (clk_2f),
        .reset_L    (reset_L),
        .selector   (clk_f),
        .data_in0   (data_in0[7:0]),
        .data_in1   (data_in1[7:0]),
        .valid_0    (valid_in0),
        .valid_1    (valid_in1),
        .valid_out  (valid_out0_toL2),
        .data_out_cond (out0_toL2[7:0])
    );

    mux21_8b_cond mux1(
        .clk        (clk_2f),
        .reset_L    (reset_L),
        .selector   (clk_f),
        .data_in0   (data_in2[7:0]),
        .data_in1   (data_in3[7:0]),
        .valid_0    (valid_in2),
        .valid_1    (valid_in3),
        .valid_out  (valid_out1_toL2),
        .data_out_cond (out1_toL2[7:0])
    );
endmodule