// `include "recirc.v"
// `include "L1_cond.v"
// `include "L2_cond.v"
// `include "PS_COM_cond.v"
// `include "SP_IDL_cond.v"
module phy_tx_cond (
               input        clk,
               input        clk_2f,
               input        clk_4f,
               input        clk_16f,
               input        clk_32f,
               input        reset_L,
               input [7:0]  data_in0,
               input [7:0]  data_in1,
               input [7:0]  data_in2,
               input [7:0]  data_in3,
               input        valid_0,
               input        valid_1,
               input        valid_2,
               input        valid_3,
               input        PS_IDL_out,
               output    	phy_tx_out,
               output     	prob_valid0,
               output     	prob_valid1,
               output     	prob_valid2,
               output     	prob_valid3,
               output  		[7:0] prob_in0,
               output  		[7:0] prob_in1,
               output  		[7:0] prob_in2,
               output  		[7:0] prob_in3
              );
    
    wire [7:0] L1_in0, L1_in1, L1_in2, L1_in3;
    wire L1_valid_0, L1_valid_1, L1_valid_2, L1_valid_3;
    wire [7:0] out0_toL2, out1_toL2;
    wire valid_out0_toL2, valid_out1_toL2;
    wire [7:0] data_out_cond;
    wire valid_out, IDL;

    recirc recirculacion(/*AUTOINST*/
			 // Outputs
			 .L1_in0		(L1_in0[7:0]),
			 .L1_valid_0		(L1_valid_0),
			 .L1_in1		(L1_in1[7:0]),
			 .L1_valid_1		(L1_valid_1),
			 .L1_in2		(L1_in2[7:0]),
			 .L1_valid_2		(L1_valid_2),
			 .L1_in3		(L1_in3[7:0]),
			 .L1_valid_3		(L1_valid_3),
			 .prob_in0		(prob_in0[7:0]),
			 .prob_valid0		(prob_valid0),
			 .prob_in1		(prob_in1[7:0]),
			 .prob_valid1		(prob_valid1),
			 .prob_in2		(prob_in2[7:0]),
			 .prob_valid2		(prob_valid2),
			 .prob_in3		(prob_in3[7:0]),
			 .prob_valid3		(prob_valid3),
			 // Inputs
			 .clk			(clk),
			 .IDL			(IDL),
			 .data_in0		(data_in0[7:0]),
			 .valid_0		(valid_0),
			 .data_in1		(data_in1[7:0]),
			 .valid_1		(valid_1),
			 .data_in2		(data_in2[7:0]),
			 .valid_2		(valid_2),
			 .data_in3		(data_in3[7:0]),
			 .valid_3		(valid_3));

    L1_cond L1(/*AUTOINST*/
	       // Outputs
	       .valid_out0_toL2		(valid_out0_toL2),
	       .valid_out1_toL2		(valid_out1_toL2),
	       .out0_toL2		(out0_toL2[7:0]),
	       .out1_toL2		(out1_toL2[7:0]),
	       // Inputs
	       .clk_2f			(clk_2f),
	       .clk_f			(clk),
	       .reset_L			(reset_L),
	       .data_in0		(L1_in0[7:0]),
	       .data_in1		(L1_in1[7:0]),
	       .data_in2		(L1_in2[7:0]),
	       .data_in3		(L1_in3[7:0]),
	       .valid_in0		(L1_valid_0),
	       .valid_in1		(L1_valid_1),
	       .valid_in2		(L1_valid_2),
	       .valid_in3		(L1_valid_3));

    L2_cond L2(/*AUTOINST*/
	       // Outputs
	       .valid_out		(valid_out),
	       .data_out_cond		(data_out_cond[7:0]),
	       // Inputs
	       .clk			(clk_4f),
	       .selector		(clk_2f),
	       .reset_L			(reset_L),
	       .data_in0		(out0_toL2[7:0]),
	       .data_in1		(out1_toL2[7:0]),
	       .valid_0			(valid_out0_toL2),
	       .valid_1			(valid_out1_toL2));

    PS_COM_cond PS_COM(
            .clk_4f(clk_4f),
            .clk_32f(clk_32f),
            .valid_in(valid_out),
            .reset_L(reset_L),
            .data_in(data_out_cond[7:0]),
            .data_out(phy_tx_out));

    SP_IDL_cond SP_IDL(
            .clk_32f(clk_32f),
            .reset_L(reset_L),
            .data_in(PS_IDL_out),
            .IDLE_out(IDL)
    );

endmodule
