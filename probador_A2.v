module probador_A2 (
                   output reg       clk_4f,
                   output reg       clk_32f,
                   output reg       valid_in,
                   output reg       activo,
                   output reg       ready_L,
                   output reg       ready_L2,
                   output reg       reset_L,
                   output reg [7:0] data_in,
                   
                   input  data_out
                   );

    reg clk_8f, clk_16f;
    
    initial begin
        $dumpfile("resultados_PS_SP.vcd");
        $dumpvars;


        {valid_in, data_in} = 9'b0; // Son 9 bits
        reset_L = 0;
        ready_L = 1;
        ready_L2 = 1;
        activo = 1'b0;


        @ (posedge clk_4f);
		@ (posedge clk_4f);
        reset_L <= 1;
        @ (posedge clk_4f);
		@ (posedge clk_4f);
        
        #8
        ready_L2 <= 0;
        #8;
        ready_L <= 0;
        @ (posedge clk_4f);
        
		@ (posedge clk_4f);

        valid_in <= 1;
        data_in <= 8'hFF;
        
        @ (posedge clk_4f);
        data_in <= 8'hEE;

        @ (posedge clk_4f);
        data_in <= 8'hBC;
        activo <= 1'b1;
        @ (posedge clk_4f);
        data_in <= 8'hBC;

        @ (posedge clk_4f);
        data_in <= 8'hFF;

        @ (posedge clk_4f);
        data_in <= 8'hEE;

        @ (posedge clk_4f);
        data_in <= 8'hBC;

        @ (posedge clk_4f);
        @ (posedge clk_4f);
    
	    $finish;
	end

	always @ (posedge clk_32f) begin
		clk_16f <= ~clk_16f;
	end

	always @ (posedge clk_16f) begin
		clk_8f <= ~clk_8f;
	end

    always @ (posedge clk_8f) begin
		clk_4f <= ~clk_4f;
	end


    initial clk_4f <= 0;
	initial clk_8f <= 0;
	initial clk_16f <= 0;
    initial clk_32f <= 0;


    always #4 clk_32f <= ~clk_32f;


endmodule