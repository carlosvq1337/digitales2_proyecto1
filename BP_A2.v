`timescale 1ns/100ps

`include "PS_COM_cond.v"
`include "PS_IDL_cond.v"
`include "SP_COM_cond.v"
`include "SP_IDL_cond.v"

`include "PS_COM_estruc.v"
`include "PS_IDL_estruc.v"
`include "SP_COM_estruc.v"
`include "SP_IDL_estruc.v"

`include "probador_A2.v"
`include "cmos_cells.v"


module BancoPruebas; 

    wire ready_L, ready_L2, activo, reset_L, clk_4f, clk_32f, valid_in, data_out_PSCOM, active, valid_out, data_out_PSIDL, IDLE_out, data_out_PSCOM_estruc, data_out_PSIDL_estruc, valid_out_estruc, IDLE_out_estruc;
    wire [7:0] data_in, data_out, data_out_estruc;

    PS_COM_cond     PS_COM_cond_(
                                    .clk_4f         (clk_4f),
                                    .clk_32f        (clk_32f),
                                    .reset_L        (reset_L),
                                    .valid_in       (valid_in),
                                    .data_out       (data_out_PSCOM),
                                    .data_in        (data_in[7:0])
                                );

    SP_COM_cond     SP_COM_cond_(
                                .valid_out      (valid_out_estruc),
                                .clk_32f        (clk_32f),
                                .reset_L        (reset_L),
                                .active         (active),
                                .data_out       (data_out),
                                .data_in        (data_out_PSCOM)
                                 );

    PS_IDL_cond     PS_IDL_cond_(
                                .clk_4f         (clk_4f),
                                .clk_32f        (clk_32f),
                                .reset_L        (reset_L),
                                .active         (activo),
                                .data_out       (data_out_PSIDL)
                            );

    SP_IDL_cond     SP_IDL_cond_(
                                .reset_L        (reset_L),
                                .clk_32f        (clk_32f),
                                .data_in        (data_out_PSIDL),
                                .IDLE_out       (IDLE_out)
                                 );


    PS_COM_estruc     PS_COM_estruc_(
                                .clk_4f         (clk_4f),
                                .clk_32f        (clk_32f),
                                .reset_L        (reset_L),
                                .valid_in       (valid_in),
                                .data_out       (data_out_PSCOM_estruc),
                                .data_in        (data_in[7:0])
                                );



    SP_COM_estruc     SP_COM_estruc_(
                                .valid_out      (valid_out_estruc),
                                .clk_32f        (clk_32f),
                                .reset_L        (reset_L),
                                .active         (active),
                                .data_out       (data_out_estruc),
                                .data_in        (data_out_PSCOM_estruc)
                                 );

    PS_IDL_estruc     PS_IDL_estruc_(
                                .clk_4f         (clk_4f),
                                .clk_32f        (clk_32f),
                                .active         (activo),
                                .reset_L        (reset_L),
                                .data_out       (data_out_PSIDL_estruc)
                            );

    SP_IDL_estruc     SP_IDL_estruc_(
                                .reset_L        (reset_L),
                                .clk_32f        (clk_32f),
                                .data_in        (data_out_PSIDL_estruc),
                                .IDLE_out       (IDLE_out_estruc)
                                 );

    probador_A2     probador_A2_(
                                    .clk_4f         (clk_4f),
                                    .reset_L        (reset_L),
                                    .clk_32f        (clk_32f),
                                    .ready_L        (ready_L),
                                    .activo         (activo),
                                    .valid_in       (valid_in),
                                    .data_out       (data_out_PSCOM),
                                    .data_in        (data_in[7:0])
                                );

endmodule