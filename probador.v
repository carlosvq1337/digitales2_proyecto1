module probador (
                   output reg       clk_4f,
                   output reg       clk_2f,
                   output reg       clk_f,
                   output reg       reset_L,
                   output reg [7:0] data_0,
                   output reg [7:0] data_1,
                   output reg [7:0] data_2,
                   output reg [7:0] data_3,
                   output reg       valid_0,
                   output reg       valid_1,
                   output reg       valid_2,
                   output reg       valid_3,
                   output reg IDL,
                   
                   input  prob_valid0,
                   input  prob_valid1,
                   input  prob_valid2,
                   input  prob_valid3,
                   input  [7:0] prob_in0,
                   input  [7:0] prob_in1,
                   input  [7:0] prob_in2,
                   input  [7:0] prob_in3
                   );
    initial begin
        $dumpfile("resultados_probador.vcd");
        $dumpvars;



	data_0 <= 8'b0000;
    data_1 <= 8'b0000;
    data_2 <= 8'b0000;
    data_3 <= 8'b0000;
    reset_L <= 1'b0;

      // valid values set to 0
    valid_0 <= 0;
    valid_1 <= 0;
    valid_2 <= 0;
    valid_3 <= 0;


    @ (posedge clk_f);
    @ (posedge clk_f);

	
	reset_L <= 1;

    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);

    data_0 <= 8'hFF;
    data_1 <= 8'hEE;
    data_2 <= 8'hDD;
    data_3 <= 8'hCC;
	valid_0 <= 1;
    valid_1 <= 1;
    valid_2 <= 1;
    valid_3 <= 1;

	@ (posedge clk_f);
	data_0 <= 8'hBB;
    data_1 <= 8'hAA;
    data_2 <= 8'h99;
    data_3 <= 8'h88;

	@ (posedge clk_f);
	data_0 <= 8'b0000;
    data_1 <= 8'b0000;
    data_2 <= 8'h77;
    data_3 <= 8'b0000;
	valid_0 <= 0;
    valid_1 <= 0;
    valid_3 <= 0;

	@ (posedge clk_f);
	data_2 <= 8'b0000;
	valid_2 <= 0;

	@ (posedge clk_f);


    @ (posedge clk_f);
	data_0 <= 8'hFF;
    data_1 <= 8'hEE;
    data_2 <= 8'hDD;
    data_3 <= 8'hCC;
	valid_0 <= 1;
    valid_1 <= 1;
    valid_2 <= 1;
    valid_3 <= 1;
	reset_L <= 1;

	@ (posedge clk_f);
	data_0 <= 8'hBB;
    data_1 <= 8'hAA;
    data_2 <= 8'h99;
    data_3 <= 8'h88;

	@ (posedge clk_f);
	data_0 <= 8'b0000;
    data_1 <= 8'b0000;
    data_2 <= 8'h77;
    data_3 <= 8'b0000;
	valid_0 <= 0;
    valid_1 <= 0;
    valid_3 <= 0;

	@ (posedge clk_f);
	data_2 <= 8'b0000;
	valid_2 <= 0;

    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    


	$finish;
	end

	always @ (posedge clk_4f) begin
		clk_2f <= ~clk_2f;
	end

	always @ (posedge clk_2f) begin
		clk_f <= ~clk_f;
	end


    initial clk_f <= 0;
	initial clk_2f <= 0;
	initial clk_4f <= 0;


    always #4 clk_4f <= ~clk_4f;


endmodule
