// `include "DL1_cond.v"
// `include "DL2_cond.v"
// `include "PS_IDL_cond.v"
// `include "SP_COM_cond.v"
module phy_rx_cond( input        clk,
                    input        clk_2f,
                    input        clk_4f,
                    input        clk_32f,
                    input        reset_L,
                    input        phy_tx_out,
                    output      PS_IDL_out,
                    output         valid_out0,
                    output         valid_out1,
                    output         valid_out2,
                    output         valid_out3,
                    output   [7:0] DL1_data_out0,
                    output   [7:0] DL1_data_out1,
                    output   [7:0] DL1_data_out2,
                    output   [7:0] DL1_data_out3
);

    wire valid_SP_COM, active, DL2_valid_out0, DL2_valid_out1;
    wire [7:0] DL2_in, DL2_out0, DL2_out1;
    
    SP_COM_cond SP_COM(
        .clk_32f(clk_32f),
        .reset_L(reset_L),
        .data_in(phy_tx_out),
        .valid_out(valid_SP_COM),
        .active(active),
        .data_out(DL2_in));
    
    PS_IDL_cond PS_IDL(
        .clk_4f(clk_4f),
        .clk_32f(clk_32f),
        .reset_L(reset_L),
        .active(active),
        .data_out(PS_IDL_out));

    DL2_cond DL2(
        .clk(clk_4f),
        .reset_L(reset_L),
        .data_in(DL2_in),
        .valid_in(valid_SP_COM),
        .valid_out0(DL2_valid_out0),
        .valid_out1(DL2_valid_out1),
        .data_out0(DL2_out0[7:0]),
        .data_out1(DL2_out1[7:0])
    );

    DL1_cond DL1(
        .clk(clk_2f),
        .reset_L(reset_L),
        .data_in0(DL2_out0),
        .data_in1(DL2_out1),
        .valid_in0(DL2_valid_out0),
        .valid_in1(DL2_valid_out1),
        .valid_out0(valid_out0),
		.valid_out1(valid_out1),
		.valid_out2(valid_out2),
		.valid_out3(valid_out3),
		.data_out0(DL1_data_out0),
		.data_out1(DL1_data_out1),
		.data_out2(DL1_data_out2),
		.data_out3(DL1_data_out3)
    );

endmodule