module PS_COM_cond (
                    input       clk_4f,
                    input       clk_32f,
                    input       valid_in,
                    input       reset_L,
                    input [7:0] data_in,
                    output reg  data_out
                    );


    reg [2:0] indice_dato;

    always @ (posedge clk_32f) begin
    if (reset_L == 1) begin
        if (valid_in == 1) begin
            case (indice_dato)
                0: data_out <= data_in[7];
                1: data_out <= data_in[6];
                2: data_out <= data_in[5];
                3: data_out <= data_in[4];
                4: data_out <= data_in[3];
                5: data_out <= data_in[2];
                6: data_out <= data_in[1];
                7: data_out <= data_in[0];
            endcase
            indice_dato <= indice_dato +1;
            if (indice_dato == 7)begin
                indice_dato <= 0;
            end
        end else begin
            case (indice_dato)   //hex(BC)
                0: data_out <= 1;
                1: data_out <= 0;
                2: data_out <= 1;
                3: data_out <= 1;
                4: data_out <= 1;
                5: data_out <= 1;
                6: data_out <= 0;
                7: data_out <= 0;
            endcase
            indice_dato <= indice_dato +1;
            if (indice_dato == 7)begin
                indice_dato <= 0;
            end
        end
    end else begin
        data_out <= 0;
        indice_dato <= 0;
    end
end

endmodule