`timescale 1ns/100ps
// escala	unidad temporal (valor de "#1") / precisi�n

`include "L1_cond.v"
`include "DL1_cond.v"
`include "L2_cond.v"
`include "DL2_cond.v"

`include "probador.v"
`include "cmos_cells.v"
`include "recirc.v"
`include "dmux12_8b_cond.v"
`include "mux21_8b_cond.v"

`include "PS_COM_cond.v"
`include "PS_IDL_cond.v"
`include "SP_COM_cond.v"
`include "SP_IDL_cond.v"

`include "phy_cond.v"
`include "phy_rx_cond.v"
`include "phy_tx_cond.v"

// phy_estruc.v incluye todos los submodulos estructurales
`include "phy_estruc.v"

`include "probador_final.v"

module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.

	//CONDUCTUALES

	//salidas del probador
	wire [7:0] data_in0, data_in1, data_in2, data_in3;
	wire valid_0, valid_1, valid_2, valid_3, reset_L, clk_f, clk_2f, clk_4f, clk_16f, clk_32f;

    //entradas al probador, desde la recirculación
    wire [7:0] prob_in0, prob_in1, prob_in2, prob_in3;
    wire prob_valid0, prob_valid1, prob_valid2, prob_valid3;

    //entradas al probador, desde DL1
    wire [7:0] DL1_data_out0, DL1_data_out1, DL1_data_out2, DL1_data_out3;
    wire valid_out0, valid_out1, valid_out2, valid_out3;


	//ESTRUCTURALES

    //entradas al probador, desde la recirculación
    wire [7:0] prob_in0_estruc, prob_in1_estruc, prob_in2_estruc, prob_in3_estruc;
    wire prob_valid0_estruc, prob_valid1_estruc, prob_valid2_estruc, prob_valid3_estruc;

    //entradas al probador, desde DL1
    wire [7:0] data_out0_estruc, data_out1_estruc, data_out2_estruc, data_out3_estruc;
    wire valid_out0_estruc, valid_out1_estruc, valid_out2_estruc, valid_out3_estruc;

	phy_cond				phy_cond_(
                                .reset_L            (reset_L),

                                .clk                (clk_f),
                                .clk_2f             (clk_2f),
                                .clk_4f             (clk_4f),
                                .clk_16f            (clk_16f),
                                .clk_32f            (clk_32f),

                                .data_in0           (data_in0),
                                .data_in1           (data_in1),
                                .data_in2           (data_in2),
                                .data_in3           (data_in3),

                                .valid_0            (valid_0),
                                .valid_1            (valid_1),
                                .valid_2            (valid_2),
                                .valid_3            (valid_3),

                                .prob_in0           (prob_in0),
                                .prob_in1           (prob_in1),
                                .prob_in2           (prob_in2),
                                .prob_in3           (prob_in3),

                                .prob_valid0        (prob_valid0),
                                .prob_valid1        (prob_valid1),
                                .prob_valid2        (prob_valid2),
                                .prob_valid3        (prob_valid3),

                                .DL1_data_out0      (DL1_data_out0),
                                .DL1_data_out1      (DL1_data_out1),
                                .DL1_data_out2      (DL1_data_out2),
                                .DL1_data_out3      (DL1_data_out3),

                                .valid_out0         (valid_out0),
                                .valid_out1         (valid_out1),
                                .valid_out2         (valid_out2),
                                .valid_out3         (valid_out3)
	);

    phy_estruc  phy_estruc_
        (/*AUTOINST*/
	 // Outputs
	 .DL1_data_out0			(data_out0_estruc[7:0]),
	 .DL1_data_out1			(data_out1_estruc[7:0]),
	 .DL1_data_out2			(data_out2_estruc[7:0]),
	 .DL1_data_out3			(data_out3_estruc[7:0]),
	 .prob_in0			(prob_in0_estruc[7:0]),
	 .prob_in1			(prob_in1_estruc[7:0]),
	 .prob_in2			(prob_in2_estruc[7:0]),
	 .prob_in3			(prob_in3_estruc[7:0]),
	 .prob_valid0			(prob_valid0_estruc),
	 .prob_valid1			(prob_valid1_estruc),
	 .prob_valid2			(prob_valid2_estruc),
	 .prob_valid3			(prob_valid3_estruc),
	 .valid_out0			(valid_out0_estruc),
	 .valid_out1			(valid_out1_estruc),
	 .valid_out2			(valid_out2_estruc),
	 .valid_out3			(valid_out3_estruc),
	 // Inputs
	 .clk				(clk_f),
	 .clk_16f			(clk_16f),
	 .clk_2f			(clk_2f),
	 .clk_32f			(clk_32f),
	 .clk_4f			(clk_4f),
	 .data_in0			(data_in0[7:0]),
	 .data_in1			(data_in1[7:0]),
	 .data_in2			(data_in2[7:0]),
	 .data_in3			(data_in3[7:0]),
	 .reset_L			(reset_L),
	 .valid_0			(valid_0),
	 .valid_1			(valid_1),
	 .valid_2			(valid_2),
	 .valid_3			(valid_3));

    probador_final                prob(
                                .reset_L            (reset_L),

                                .clk_f              (clk_f),
                                .clk_2f             (clk_2f),
                                .clk_4f             (clk_4f),
                                .clk_16f            (clk_16f),
                                .clk_32f            (clk_32f),

                                .data_in0           (data_in0),
                                .data_in1           (data_in1),
                                .data_in2           (data_in2),
                                .data_in3           (data_in3),

                                .valid_0            (valid_0),
                                .valid_1            (valid_1),
                                .valid_2            (valid_2),
                                .valid_3            (valid_3),

                                .prob_in0           (prob_in0),
                                .prob_in1           (prob_in1),
                                .prob_in2           (prob_in2),
                                .prob_in3           (prob_in3),
                                .prob_in0_estruc           (prob_in0_estruc),
                                .prob_in1_estruc           (prob_in1_estruc),
                                .prob_in2_estruc           (prob_in2_estruc),
                                .prob_in3_estruc           (prob_in3_estruc),

                                .prob_valid0        (prob_valid0),
                                .prob_valid1        (prob_valid1),
                                .prob_valid2        (prob_valid2),
                                .prob_valid3        (prob_valid3),
                                .prob_valid0_estruc         (prob_valid0_estruc),
                                .prob_valid1_estruc         (prob_valid1_estruc),
                                .prob_valid2_estruc         (prob_valid2_estruc),
                                .prob_valid3_estruc         (prob_valid3_estruc),

                                .DL1_data_out0      (DL1_data_out0),
                                .DL1_data_out1      (DL1_data_out1),
                                .DL1_data_out2      (DL1_data_out2),
                                .DL1_data_out3      (DL1_data_out3),
                                .data_out0_estruc      (data_out0_estruc),
                                .data_out1_estruc      (data_out1_estruc),
                                .data_out2_estruc      (data_out2_estruc),
                                .data_out3_estruc      (data_out3_estruc),

                                .valid_out0         (valid_out0),
                                .valid_out1         (valid_out1),
                                .valid_out2         (valid_out2),
                                .valid_out3         (valid_out3),
                                .valid_out0_estruc         (valid_out0_estruc),
                                .valid_out1_estruc         (valid_out1_estruc),
                                .valid_out2_estruc         (valid_out2_estruc),
                                .valid_out3_estruc         (valid_out3_estruc)
    );


// Local Variables:
// verilog-library-directories:("phy")
// End:

endmodule
