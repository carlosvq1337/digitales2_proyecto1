module probador_final (
                   output reg       clk_f,
                   output reg       clk_2f,
                   output reg       clk_4f,
                   output reg       clk_16f,
                   output reg       clk_32f,

                   output reg       reset_L,

                   output reg    [7:0]    data_in0,
                   output reg    [7:0]    data_in1,
                   output reg    [7:0]    data_in2,
                   output reg    [7:0]    data_in3,

                   output reg       valid_0,
                   output reg       valid_1,
                   output reg       valid_2,
                   output reg       valid_3,

                   input [7:0]      prob_in0,
                   input [7:0]      prob_in1,
                   input [7:0]      prob_in2,
                   input [7:0]      prob_in3,
                   input [7:0]      prob_in0_estruc,
                   input [7:0]      prob_in1_estruc,
                   input [7:0]      prob_in2_estruc,
                   input [7:0]      prob_in3_estruc,

                   input            prob_valid0,
                   input            prob_valid1,
                   input            prob_valid2,
                   input            prob_valid3,
                   input            prob_valid0_estruc,
                   input            prob_valid1_estruc,
                   input            prob_valid2_estruc,
                   input            prob_valid3_estruc,

                   input  [7:0]         DL1_data_out0,
                   input  [7:0]         DL1_data_out1,
                   input  [7:0]         DL1_data_out2,
                   input  [7:0]         DL1_data_out3,
                   input  [7:0]         data_out0_estruc,
                   input  [7:0]         data_out1_estruc,
                   input  [7:0]         data_out2_estruc,
                   input  [7:0]         data_out3_estruc,

                   input            valid_out0,
                   input            valid_out1,
                   input            valid_out2,
                   input            valid_out3,
                   input            valid_out0_estruc,
                   input            valid_out1_estruc,
                   input            valid_out2_estruc,
                   input            valid_out3_estruc
                   );

reg clk_8f;
    
initial begin
    $dumpfile("resultados_finales.vcd");
    $dumpvars;


    data_in0 <= 8'b0000;
    data_in1 <= 8'b0000;
    data_in2 <= 8'b0000;
    data_in3 <= 8'b0000;
    reset_L <= 1'b0;
    valid_0 <= 0;
    valid_1 <= 0;
    valid_2 <= 0;
    valid_3 <= 0;

    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);

    reset_L <= 1;

    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);

    valid_0 <= 1;
    valid_1 <= 1;
    valid_2 <= 1;
    valid_3 <= 1;

	data_in0 <= 8'hFF;
    data_in1 <= 8'hEE;
    data_in2 <= 8'hDD;
    data_in3 <= 8'hCC;


	@ (posedge clk_f);
	data_in0 <= 8'hBB;
    data_in1 <= 8'hAA;
    data_in2 <= 8'h99;
    data_in3 <= 8'h88;

	@ (posedge clk_f);
	data_in0 <= 8'b0000;
    data_in1 <= 8'b0000;
    data_in2 <= 8'h77;
    data_in3 <= 8'b0000;
	valid_0 <= 0;
    valid_1 <= 0;
    valid_3 <= 0;

	@ (posedge clk_f);
	data_in2 <= 8'b0000;
	valid_2 <= 0;

	@ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    @ (posedge clk_f);
    
	    $finish;
	end

	always @ (posedge clk_32f) begin
		clk_16f <= ~clk_16f;
	end

	always @ (posedge clk_16f) begin
		clk_8f <= ~clk_8f;
	end

    always @ (posedge clk_8f) begin
		clk_4f <= ~clk_4f;
	end

    always @ (posedge clk_4f) begin
		clk_2f <= ~clk_2f;
	end

    always @ (posedge clk_2f) begin
		clk_f <= ~clk_f;
	end

    initial clk_f <= 0;
    initial clk_2f <= 0;
    initial clk_4f <= 0;
	initial clk_8f <= 0;
	initial clk_16f <= 0;
    initial clk_32f <= 0;


    always #4 clk_32f <= ~clk_32f;


endmodule