module recirc( input clk,
                input IDL,
                input [7:0] data_in0,
                input valid_0,
                input [7:0] data_in1,
                input valid_1,
                input [7:0] data_in2,
                input valid_2,
                input [7:0] data_in3,
                input valid_3,
                output reg [7:0] L1_in0,
                output reg L1_valid_0,
                output reg [7:0] L1_in1,
                output reg L1_valid_1,
                output reg [7:0] L1_in2,
                output reg L1_valid_2,
                output reg [7:0] L1_in3,
                output reg L1_valid_3,
                output reg [7:0] prob_in0,
                output reg prob_valid0,
                output reg [7:0] prob_in1,
                output reg prob_valid1,
                output reg [7:0] prob_in2,
                output reg prob_valid2,
                output reg [7:0] prob_in3,
                output reg prob_valid3
            );

    always @(*) begin
        if (IDL == 0) begin
            L1_in0 = 0;
            L1_valid_0 = 0;
            L1_in1 = 0;
            L1_valid_1 = 0;
            L1_in2 = 0;
            L1_valid_2 = 0;
            L1_in3 = 0;
            L1_valid_3 = 0;
            prob_in0 = data_in0;
            prob_valid0 = valid_0;
            prob_in1 = data_in1;
            prob_valid1 = valid_1;
            prob_in2 = data_in2;
            prob_valid2 = valid_2;
            prob_in3 = data_in3;
            prob_valid3 = valid_3;
        end else begin
            prob_in0 = 0;
            prob_valid0 = 0;
            prob_in1 = 0;
            prob_valid1 = 0;
            prob_in2 = 0;
            prob_valid2 = 0;
            prob_in3 = 0;
            prob_valid3 = 0;
            L1_in0 = data_in0;
            L1_valid_0 = valid_0;
            L1_in1 = data_in1;
            L1_valid_1 = valid_1;
            L1_in2 = data_in2;
            L1_valid_2 = valid_2;
            L1_in3 = data_in3;
            L1_valid_3 = valid_3;
        end
    end

endmodule