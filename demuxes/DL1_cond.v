module DL1_cond (	input clk,
					input reset_L,
					input [7:0] data_in0,
					input [7:0] data_in1,
					input valid_in0,
					input valid_in1,
					output valid_out0,
					output valid_out1,
					output valid_out2,
					output valid_out3,
					output [7:0] data_out0,
					output [7:0] data_out1,
					output [7:0] data_out2,
					output [7:0] data_out3
				);

	dmux12_8b_cond		DMUX0(
								.clk			(clk),
								.reset_L		(reset_L),
								.valid_in 		(valid_in0),
								.data_in 		(data_in0[7:0]),
								.data_out0		(data_out0[7:0]),
								.data_out1		(data_out1[7:0]),
								.valid_out0 	(valid_out0),
								.valid_out1 	(valid_out1)

	);

	dmux12_8b_cond		DMUX1(
								.clk			(clk),
								.reset_L		(reset_L),
								.valid_in 		(valid_in1),
								.data_in 		(data_in1[7:0]),
								.data_out0		(data_out2[7:0]),
								.data_out1		(data_out3[7:0]),
								.valid_out0 	(valid_out2),
								.valid_out1 	(valid_out3)

	);

endmodule