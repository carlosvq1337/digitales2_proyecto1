module dmux12_8b_cond(   input clk,
                            input reset_L,
                            output reg [7:0] data_out0,
                            output reg valid_out0,
                            output reg[7:0] data_out1,
                            output reg valid_out1,
                            input valid_in,
                            input [7:0] data_in
                        );

    reg [7:0] data_recordar0;
    reg [7:0] data_recordar1;
    reg [7:0] data_recordar0d;
    reg valid_recordar0;
    reg valid_recordar0d;
    reg valid_recordar1;
    reg selector;
    reg selector_active;

    always @(*) begin

        
        if (selector == 0 && valid_in == 1) begin
            data_recordar0 = data_in;
            data_recordar1 = 0;
            valid_recordar0 = valid_in;
            valid_recordar1 = 0;
            selector_active = 1;
        end else if (selector == 1 && valid_in == 1) begin
            data_recordar0 = 0;
            data_recordar1 = data_in;
            valid_recordar0 = 0;
            valid_recordar1 = valid_in;
            selector_active = 1;

        end else begin
            data_recordar0 = 0;
            data_recordar1 = 0;
            valid_recordar0 = 0;
            valid_recordar1 = 0;
            if(valid_out0 == 0 && valid_out1 == 0) begin
                selector_active = 0;               
            end else begin
                selector_active = 1;
            end

        end
    end

    always @(posedge clk) begin
        if (reset_L == 1) begin
            if (selector == 0) begin
                data_recordar0d <= data_recordar0;
                valid_recordar0d <= valid_recordar0;
            end else if (selector == 1) begin
                data_out1 <= data_recordar1;
                valid_out1 <= valid_recordar1;
            end
            if (selector_active == 1) begin
                selector <= ~selector;
            end
        data_out0 <= data_recordar0d;
        valid_out0 <= valid_recordar0d;
        end else begin
            data_out0 <= 0;
            data_recordar0d <= 0;
            valid_recordar0d <= 0;
            data_out1 <= 0;
            valid_out0 <= 0;
            valid_out1 <= 0;
            selector <= 0;
        end
    end

endmodule
