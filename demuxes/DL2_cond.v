module DL2_cond (	input clk,
					input reset_L,
					input [7:0] data_in,
					input valid_in,
					output valid_out0,
					output valid_out1,
					output [7:0] data_out0,
					output [7:0] data_out1
				);

	dmux12_8b_cond		DMUX0(
								.clk			(clk),
								.reset_L		(reset_L),
								.valid_in 		(valid_in),
								.data_in 		(data_in[7:0]),
								.data_out0		(data_out0[7:0]),
								.data_out1		(data_out1[7:0]),
								.valid_out0 	(valid_out0),
								.valid_out1 	(valid_out1)

	);
endmodule