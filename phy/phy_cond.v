//`include "phy_tx_cond.v"
//`include "phy_rx_cond.v"
module phy_cond(
        input clk,
        input clk_2f,
        input clk_4f,
        input clk_16f,
        input clk_32f,
        input reset_L,
        input [7:0] data_in0,
        input [7:0] data_in1,
        input [7:0] data_in2,
        input [7:0] data_in3,
        input valid_0,
        input valid_1,
        input valid_2,
        input valid_3,
        output      valid_out0,
        output      valid_out1,
        output      valid_out2,
        output      valid_out3,
        output   [7:0]   DL1_data_out0,
        output   [7:0]   DL1_data_out1,
        output   [7:0]   DL1_data_out2,
        output   [7:0]   DL1_data_out3,
        output      prob_valid0,
        output      prob_valid1,
        output      prob_valid2,
        output      prob_valid3,
        output   [7:0] prob_in0,
        output   [7:0] prob_in1,
        output   [7:0] prob_in2,
        output   [7:0] prob_in3
);

    wire PS_IDL_out, phy_tx_out;

    phy_tx_cond phy_tx(/*AUTOINST*/
		       // Outputs
		       .phy_tx_out	(phy_tx_out),
		       .prob_valid0	(prob_valid0),
		       .prob_valid1	(prob_valid1),
		       .prob_valid2	(prob_valid2),
		       .prob_valid3	(prob_valid3),
		       .prob_in0	(prob_in0[7:0]),
		       .prob_in1	(prob_in1[7:0]),
		       .prob_in2	(prob_in2[7:0]),
		       .prob_in3	(prob_in3[7:0]),
		       // Inputs
		       .clk		(clk),
		       .clk_2f		(clk_2f),
		       .clk_4f		(clk_4f),
		       .clk_16f		(clk_16f),
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L),
		       .data_in0	(data_in0[7:0]),
		       .data_in1	(data_in1[7:0]),
		       .data_in2	(data_in2[7:0]),
		       .data_in3	(data_in3[7:0]),
		       .valid_0		(valid_0),
		       .valid_1		(valid_1),
		       .valid_2		(valid_2),
		       .valid_3		(valid_3),
		       .PS_IDL_out	(PS_IDL_out));

    phy_rx_cond phy_rx(/*AUTOINST*/
		       // Outputs
		       .PS_IDL_out	(PS_IDL_out),
		       .valid_out0	(valid_out0),
		       .valid_out1	(valid_out1),
		       .valid_out2	(valid_out2),
		       .valid_out3	(valid_out3),
		       .DL1_data_out0	(DL1_data_out0[7:0]),
		       .DL1_data_out1	(DL1_data_out1[7:0]),
		       .DL1_data_out2	(DL1_data_out2[7:0]),
		       .DL1_data_out3	(DL1_data_out3[7:0]),
		       // Inputs
		       .clk		(clk),
		       .clk_2f		(clk_2f),
		       .clk_4f		(clk_4f),
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L),
		       .phy_tx_out	(phy_tx_out));


endmodule
