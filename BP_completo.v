`timescale 1ns/100ps
// escala	unidad temporal (valor de "#1") / precisi�n

`include "L1_cond.v"
`include "DL1_cond.v"
`include "L2_cond.v"
`include "DL2_cond.v"
`include "probador.v"
`include "cmos_cells.v"
`include "recirc.v"
`include "dmux12_8b_cond.v"
`include "mux21_8b_cond.v"


`include "L1_estruc.v"
`include "DL1_estruc.v"
`include "L2_estruc.v"
`include "DL2_estruc.v"
`include "recirc_estruc.v"


module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.

	//CONDUCTUALES

	//salidas del probador (conductual)
	wire [7:0] data_in0, data_in1, data_in2, data_in3;
	wire IDL, valid_0, valid_1, valid_2, valid_3, reset_L;

	//para las entradas al Probador (conductual)
	wire [7:0] prob_in0, prob_in1, prob_in2, prob_in3;
	wire prob_valid0, prob_valid1, prob_valid2, prob_valid3;

	//para L1 (conductual)
	wire [7:0] L1_in0, L1_in1, L1_in2, L1_in3, L1_out0_cond, L1_out1_cond;
	wire L1_valid_0, L1_valid_1, L1_valid_2, L1_valid_3,  L1_valid_out0_cond, L1_valid_out1_cond;

	//para L2 (conductual)
	wire [7:0] L2_out_cond;
	wire L2_valid_out_cond;

	//para DL2 (conductual)
	wire [7:0] DL2_out0_cond, DL2_out1_cond;
	wire DL2_valid_out0, DL2_valid_out1;

	//para DL1 (conductual)
	wire [7:0] DL1_out0_cond, DL1_out1_cond, DL1_out2_cond, DL1_out3_cond;
	wire DL1_valid_out0, DL1_valid_out1, DL1_valid_out2, DL1_valid_out3;

	
	//ESTRUCTURALES


	//para las entradas al Probador (Estructural)
	wire [7:0] prob_in0_estruc, prob_in1_estruc, prob_in2_estruc, prob_in3_estruc;
	wire prob_valid0_estruc, prob_valid1_estruc, prob_valid2_estruc, prob_valid3_estruc;

	//para L1 (Estructural)
	wire [7:0] L1_in0_estruc, L1_in1_estruc, L1_in2_estruc, L1_in3_estruc, L1_out0_estruc, L1_out1_estruc;
	wire L1_valid_0_estruc, L1_valid_1_estruc, L1_valid_2_estruc, L1_valid_3_estruc,  L1_valid_out0_estruc, L1_valid_out1_estruc;

	//para L2 (Estructural)
	wire [7:0] L2_out_estruc;
	wire L2_valid_out_estruc;

	//para DL2 (Estructural)
	wire [7:0] DL2_out0_estruc, DL2_out1_estruc;
	wire DL2_valid_out0_estru, DL2_valid_out1_estruc;

	//para DL1 (Estructural)
	wire [7:0] DL1_out0_estruc, DL1_out1_estruc, DL1_out2_estruc, DL1_out3_estruc;
	wire DL1_valid_out0_estruc, DL1_valid_out1_estruc, DL1_valid_out2_estruc, DL1_valid_out3_estruc;


	recirc					recirc_(
							.IDL 			(IDL),
							.data_in0		(data_in0[7:0]),
							.data_in1		(data_in1[7:0]),
							.data_in2		(data_in2[7:0]),
							.data_in3		(data_in3[7:0]),
							.valid_0		(valid_0),
							.valid_1		(valid_1),
							.valid_2		(valid_2),
							.valid_3		(valid_3),
							.L1_in0 		(L1_in0),
							.L1_in1 		(L1_in1),
							.L1_in2 		(L1_in2),
							.L1_in3 		(L1_in3),
							.L1_valid_0		(L1_valid_0),
							.L1_valid_1		(L1_valid_1),
							.L1_valid_2		(L1_valid_2),
							.L1_valid_3		(L1_valid_3),
							.prob_in0		(prob_in0[7:0]),
							.prob_in1		(prob_in1[7:0]),
							.prob_in2		(prob_in2[7:0]),
							.prob_in3		(prob_in3[7:0]),
							.prob_valid0	(prob_valid0),
							.prob_valid1	(prob_valid1),
							.prob_valid2	(prob_valid2),
							.prob_valid3	(prob_valid3)
							

	);


	L1_cond				L1_cond_(
							.clk_f			(clk_f),
							.clk_2f			(clk_2f),
            				.reset_L		(reset_L),
            				.data_in0		(L1_in0),									
            				.data_in1		(L1_in1),
            				.data_in2		(L1_in2),
            				.data_in3		(L1_in3),
            				.valid_in0		(L1_valid_0),
            				.valid_in1		(L1_valid_1),
            				.valid_in2		(L1_valid_2),
            				.valid_in3		(L1_valid_3),
            				.valid_out0_toL2	(L1_valid_out0_cond),
            				.valid_out1_toL2	(L1_valid_out1_cond),
            				.out0_toL2			(L1_out0_cond[7:0]),
							.out1_toL2			(L1_out1_cond[7:0])

	);


	L2_cond		L2_cond_(

						 	.clk			(clk_4f),
							.reset_L		(reset_L),
							.selector		(clk_2f),
							.data_in0		(L1_out0_cond[7:0]),
							.data_in1		(L1_out1_cond[7:0]),
							.valid_0		(L1_valid_out0_cond),
							.valid_1		(L1_valid_out1_cond),
							.valid_out 		(L2_valid_out_cond),
							.data_out_cond		(L2_out_cond[7:0])

	);

	DL2_cond		DL2_cond_(
							.clk			(clk_4f),
							.reset_L		(reset_L),
							.selector		(clk_2f),
							.valid_in 		(L2_valid_out_cond),
							.data_in 		(L2_out_cond[7:0]),
							.data_out0		(DL2_out0_cond[7:0]),
							.data_out1		(DL2_out1_cond[7:0]),
							.valid_out0 	(DL2_valid_out0),
							.valid_out1 	(DL2_valid_out1)

	);

	DL1_cond 			DL1_cond_(
							.clk			(clk_2f),
							.selector		(clk_f),
							.reset_L		(reset_L),
							.data_in0		(DL2_out0_cond[7:0]),
							.data_in1		(DL2_out1_cond[7:0]),
							.valid_in0		(DL2_valid_out0),
							.valid_in1		(DL2_valid_out1),
							.valid_out0		(DL1_valid_out0),
							.valid_out1		(DL1_valid_out1),
							.valid_out2 	(DL1_valid_out2),
							.valid_out3		(DL1_valid_out3),
							.data_out0		(DL1_out0_cond[7:0]),
							.data_out1		(DL1_out1_cond[7:0]),
							.data_out2		(DL1_out2_cond[7:0]),
							.data_out3		(DL1_out3_cond[7:0])
	);


	recirc_estruc					recirc_estruc_(
							.IDL 			(IDL),
							.data_in0		(data_in0[7:0]),
							.data_in1		(data_in1[7:0]),
							.data_in2		(data_in2[7:0]),
							.data_in3		(data_in3[7:0]),
							.valid_0		(valid_0),
							.valid_1		(valid_1),
							.valid_2		(valid_2),
							.valid_3		(valid_3),
							.L1_in0 		(L1_in0_estruc),
							.L1_in1 		(L1_in1_estruc),
							.L1_in2 		(L1_in2_estruc),
							.L1_in3 		(L1_in3_estruc),
							.L1_valid_0		(L1_valid_0_estruc),
							.L1_valid_1		(L1_valid_1_estruc),
							.L1_valid_2		(L1_valid_2_estruc),
							.L1_valid_3		(L1_valid_3_estruc),
							.prob_in0		(prob_in0_estruc[7:0]),
							.prob_in1		(prob_in1_estruc[7:0]),
							.prob_in2		(prob_in2_estruc[7:0]),
							.prob_in3		(prob_in3_estruc[7:0]),
							.prob_valid0	(prob_valid0_estruc),
							.prob_valid1	(prob_valid1_estruc),
							.prob_valid2	(prob_valid2_estruc),
							.prob_valid3	(prob_valid3_estruc)
							

	);


	L1_estruc				L1_estruc_(
							.clk_f			(clk_f),
							.clk_2f			(clk_2f),
            				.reset_L		(reset_L),
            				.data_in0		(L1_in0_estruc),									
            				.data_in1		(L1_in1_estruc),
            				.data_in2		(L1_in2_estruc),
            				.data_in3		(L1_in3_estruc),
            				.valid_in0		(L1_valid_0_estruc),
            				.valid_in1		(L1_valid_1_estruc),
            				.valid_in2		(L1_valid_2_estruc),
            				.valid_in3		(L1_valid_3_estruc),
            				.valid_out0_toL2	(L1_valid_out0_estruc),
            				.valid_out1_toL2	(L1_valid_out1_estruc),
            				.out0_toL2			(L1_out0_estruc[7:0]),
							.out1_toL2			(L1_out1_estruc[7:0])

	);


	L2_estruc		L2_estruc_(

						 	.clk			(clk_4f),
							.reset_L		(reset_L),
							.selector		(clk_2f),
							.data_in0		(L1_out0_estruc[7:0]),
							.data_in1		(L1_out1_estruc[7:0]),
							.valid_0		(L1_valid_out0_estruc),
							.valid_1		(L1_valid_out1_estruc),
							.valid_out 		(L2_valid_out_estruc),
							.data_out_cond		(L2_out_estruc[7:0])

	);

	DL2_estruc		DL2_estruc_(
							.clk			(clk_4f),
							.reset_L		(reset_L),
							.selector		(clk_2f),
							.valid_in 		(L2_valid_out_estruc),
							.data_in 		(L2_out_estruc[7:0]),
							.data_out0		(DL2_out0_estruc[7:0]),
							.data_out1		(DL2_out1_estruc[7:0]),
							.valid_out0 	(DL2_valid_out0_estruc),
							.valid_out1 	(DL2_valid_out1_estruc)

	);

	DL1_estruc 			DL1_estruc_(
							.clk			(clk_2f),
							.selector		(clk_f),
							.reset_L		(reset_L),
							.data_in0		(DL2_out0_estruc[7:0]),
							.data_in1		(DL2_out1_estruc[7:0]),
							.valid_in0		(DL2_valid_out0_estruc),
							.valid_in1		(DL2_valid_out1_estruc),
							.valid_out0		(DL1_valid_out0_estruc),
							.valid_out1		(DL1_valid_out1_estruc),
							.valid_out2 	(DL1_valid_out2_estruc),
							.valid_out3		(DL1_valid_out3_estruc),
							.data_out0		(DL1_out0_estruc[7:0]),
							.data_out1		(DL1_out1_estruc[7:0]),
							.data_out2		(DL1_out2_estruc[7:0]),
							.data_out3		(DL1_out3_estruc[7:0])
	);


	probador 					probador_(	
							.reset_L			(reset_L),
							.clk_4f				(clk_4f),
							.IDL				(IDL),
							.clk_2f				(clk_2f),
							.clk_f				(clk_f),
							.data_0			(data_in0[7:0]),
							.data_1			(data_in1[7:0]),
							.data_2			(data_in2[7:0]),
							.data_3			(data_in3[7:0]),
							.valid_0			(valid_0),
							.valid_1			(valid_1),
							.valid_2			(valid_2),
							.valid_3			(valid_3),
							.prob_valid0		(prob_valid0),
							.prob_valid1		(prob_valid1),
							.prob_valid2		(prob_valid2),
							.prob_valid3		(prob_valid3),
							.prob_in0			(prob_in0[7:0]),
							.prob_in1			(prob_in1[7:0]),
							.prob_in2			(prob_in2[7:0]),
							.prob_in3			(prob_in3[7:0])
	);

						   

endmodule
