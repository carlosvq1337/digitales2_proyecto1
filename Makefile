#---------------------------------------------------------------------
# Input dirs, names, files
#---------------------------------------------------------------------

# Includes
IDIR= demuxes muxes recirc lib paralelo_a_serial serial_a_paralelo phy_rx phy_tx phy
INC=$(foreach d, $(IDIR), -I $d)

# Objetos
ODIR = obj
MKDIR_P = mkdir -p
_OBJ = BP_final.o

OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

# VCD
VCD = resultados_finales.vcd

# Top module
TOP = BancoPruebas


#---------------------------------------------------------------------
# rules
#---------------------------------------------------------------------
.PHONY: all gtkwave yosys clean directories

all: directories yosys iverilog gtkwave

directories: ${ODIR}

iverilog: $(VCD)

gtkwave:
	gtkwave $(VCD)


yosys:
	yosys scripts/synthL1.ys
	sed -i 's/L1_cond/L1_estruc/' muxes/L1_estruc.v
	sed -i 's/mux21_8b_cond/mux21_8b_estruc1/' muxes/L1_estruc.v
	yosys scripts/synthL2.ys
	sed -i 's/L2_cond/L2_estruc/' muxes/L2_estruc.v
	sed -i 's/mux21_8b_cond/mux21_8b_estruc2/' muxes/L2_estruc.v

	yosys scripts/synthDL1.ys
	sed -i 's/DL1_cond/DL1_estruc/' demuxes/DL1_estruc.v
	sed -i 's/dmux12_8b_cond/dmux12_8b_estruc1/' demuxes/DL1_estruc.v
	sed -i 's/dmux12_8b_cond/dmux12_8b_estruc/' demuxes/DL1_estruc.v
	yosys scripts/synthDL2.ys
	sed -i 's/DL2_cond/DL2_estruc/' demuxes/DL2_estruc.v
	sed -i 's/dmux12_8b_cond/dmux12_8b_estruc2/' demuxes/DL2_estruc.v

	yosys scripts/synth_recirc.ys
	sed -i 's/recirc/recirc_estruc/' recirc/recirc_estruc.v

	yosys scripts/synthPS_COM.ys
	sed -i 's/PS_COM_cond/PS_COM_estruc/' paralelo_a_serial/PS_COM_estruc.v
	yosys scripts/synthPS_IDL.ys
	sed -i 's/PS_IDL_cond/PS_IDL_estruc/' paralelo_a_serial/PS_IDL_estruc.v

	yosys scripts/synthSP_COM.ys
	sed -i 's/SP_COM_cond/SP_COM_estruc/' serial_a_paralelo/SP_COM_estruc.v
	yosys scripts/synthSP_IDL.ys
	sed -i 's/SP_IDL_cond/SP_IDL_estruc/' serial_a_paralelo/SP_IDL_estruc.v

	yosys scripts/synth_phy_tx.ys
	sed -i 's/phy_tx_cond/phy_tx_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/recirc/recirc_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/mux21_8b_cond/mux21_8b_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/L1_cond/L1_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/L2_cond/L2_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/PS_COM_cond/PS_COM_estruc/' phy_tx/phy_tx_estruc.v
	sed -i 's/SP_IDL_cond/SP_IDL_estruc/' phy_tx/phy_tx_estruc.v

	yosys scripts/synth_phy_rx.ys
	sed -i 's/phy_rx_cond/phy_rx_estruc/' phy_rx/phy_rx_estruc.v
	sed -i 's/dmux12_8b_cond/dmux12_8b_estruc/' phy_rx/phy_rx_estruc.v
	sed -i 's/DL1_cond/DL1_estruc/' phy_rx/phy_rx_estruc.v
	sed -i 's/DL2_cond/DL2_estruc/' phy_rx/phy_rx_estruc.v
	sed -i 's/PS_IDL_cond/PS_IDL_estruc/' phy_rx/phy_rx_estruc.v
	sed -i 's/SP_COM_cond/SP_COM_estruc/' phy_rx/phy_rx_estruc.v

	yosys scripts/synth_phy.ys
	sed -i 's/phy_cond/phy_estruc/' phy/phy_estruc.v

	sed -i 's/phy_tx_cond/phy_tx_estruc/' phy/phy_estruc.v
	sed -i 's/recirc/recirc_estruc/' phy/phy_estruc.v
	sed -i 's/mux21_8b_cond/mux21_8b_estruc/' phy/phy_estruc.v
	sed -i 's/L1_cond/L1_estruc/' phy/phy_estruc.v
	sed -i 's/L2_cond/L2_estruc/' phy/phy_estruc.v
	sed -i 's/PS_COM_cond/PS_COM_estruc/' phy/phy_estruc.v
	sed -i 's/SP_IDL_cond/SP_IDL_estruc/' phy/phy_estruc.v

	sed -i 's/phy_rx_cond/phy_rx_estruc/' phy/phy_estruc.v
	sed -i 's/dmux12_8b_cond/dmux12_8b_estruc/' phy/phy_estruc.v
	sed -i 's/DL1_cond/DL1_estruc/' phy/phy_estruc.v
	sed -i 's/DL2_cond/DL2_estruc/' phy/phy_estruc.v
	sed -i 's/PS_IDL_cond/PS_IDL_estruc/' phy/phy_estruc.v
	sed -i 's/SP_COM_cond/SP_COM_estruc/' phy/phy_estruc.v
	
clean:
	rm -f $(ODIR)/*.o
	rm -f *.vcd

$(ODIR)/%.o: %.v
	iverilog -o $@ -Ttyp $(INC) -s $(TOP) $<

$(VCD): $(OBJ)
	vvp $^

${ODIR}:
		${MKDIR_P} ${ODIR}
