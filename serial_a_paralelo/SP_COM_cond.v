module SP_COM_cond (
                    input       clk_32f,
                    input       reset_L,
                    input       data_in,
                    output reg  valid_out,
                    output reg  active,
                    output reg [7:0] data_out
                    );

    integer BC_counter;
    reg hubo_BC;
    integer p_counter;
    reg [7:0] buffer;

    always @ (posedge clk_32f) begin
        if (reset_L == 0) begin
            buffer <= 0;
            active <= 0;
            p_counter <= 0;
            BC_counter <= 0;
            valid_out <= 0;
            hubo_BC <= 0;
            data_out <= 0;
        end else begin
            buffer <= {buffer[6:0], data_in};
            if (hubo_BC == 1) begin
                p_counter <= p_counter + 1;
                if (buffer == 8'hBC) begin
                p_counter <= 0;
                BC_counter <= BC_counter + 1;
                data_out <= buffer;
                valid_out <= 0;
                    if (BC_counter >= 3) begin
                        active <= 1;
                    end
                end else if (p_counter == 7) begin
                        data_out <= buffer;
                        valid_out <= 1;
                        p_counter <= 0;
                end
            end else begin
                if (buffer == 8'hBC) begin
                BC_counter <= BC_counter + 1;
                data_out <= buffer;
                valid_out <= 0;
                hubo_BC <= 1;
                end
            end
        end
    end

endmodule